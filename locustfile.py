"""
Locust run file
"""

import json
import os
import constants
from locust import HttpLocust, TaskSet, events, stats

USERS = 0

def export_stats():
    """
    Dumps statistics to a JSON file
    """

    # Create directory if it doesn't exist
    if not os.path.isdir(constants.RESULTS_DIR):
        os.makedirs(constants.RESULTS_DIR)

    # Create one JSON report per endpoint
    for entry in stats.global_stats.entries.itervalues():
        report = dict(
            name=entry.name,
            users=USERS,
            requests=entry.num_requests,
            failures=entry.num_failures,
            fail_ratio=entry.fail_ratio,
            request_rate=entry.total_rps,
            response_time_avg=entry.avg_response_time,
            response_time_median=entry.median_response_time,
            response_time_max=entry.max_response_time,
            response_time_min=entry.min_response_time,
            content_length=entry.avg_content_length
        )

        # Dump statistics into a JSON file
        endpoint_name = entry.name.replace('/', '')
        report_name = '%s%s_%d_users.json' % (constants.REPORT_FILE_PREFIX, endpoint_name, USERS)
        report_path = os.path.join(constants.RESULTS_DIR, report_name)
        with open(report_path, 'w') as report_file:
            json.dump(report, report_file)


def endpoint1(locust):
    """
    Endpoint task
    """
    locust.client.get("/endpoint1/")

def endpoint2(locust):
    """
    Endpoint task
    """
    locust.client.get("/endpoint2/")

def endpoint3(locust):
    """
    Endpoint task
    """
    locust.client.get("/endpoint3/")

def endpoint4(locust):
    """
    Endpoint task
    """
    locust.client.get("/endpoint4/")


class TaskEndpoint1(TaskSet):
    """
    Task to access endpoint 1
    """
    tasks = {endpoint1:1}


class TaskEndpoint2(TaskSet):
    """
    Task to access endpoint 2
    """
    tasks = {endpoint2:1}


class TaskEndpoint3(TaskSet):
    """
    Task to access endpoint 3
    """
    tasks = {endpoint3:1}


class TaskEndpoint4(TaskSet):
    """
    Task to access endpoint 4
    """
    tasks = {endpoint4:1}


class TaskAllEndpoints(TaskSet):
    """
    Task to access all endpoints
    """
    tasks = {endpoint1:1, endpoint2:1, endpoint3:1, endpoint4:1}


class BaseEndpointLocust(HttpLocust):
    """
    Base class for other locust classes
    """

    min_wait = 0
    max_wait = 0
    host = constants.HOST

class Endpoint1Locust(BaseEndpointLocust):
    """
    A locust that will spam a single endpoint with requests
    """
    task_set = TaskEndpoint1


class Endpoint2Locust(BaseEndpointLocust):
    """
    A locust that will spam a single endpoint with requests
    """
    task_set = TaskEndpoint2


class Endpoint3Locust(BaseEndpointLocust):
    """
    A locust that will spam a single endpoint with requests
    """
    task_set = TaskEndpoint3


class Endpoint4Locust(BaseEndpointLocust):
    """
    A locust that will spam a single endpoint with requests
    """
    task_set = TaskEndpoint4


class AllEndpointsLocust(BaseEndpointLocust):
    """
    A locust that will spam a single endpoint with requests
    """
    task_set = TaskAllEndpoints


def on_quitting():
    """
    Exports statistics when locust is about to quit
    """

    export_stats()

def on_hatch_complete(user_count):
    """
    When all users are hatched, store the number of users for statistics
    """

    # Store user count globally simply for report file naming
    global USERS
    USERS = user_count

events.quitting += on_quitting
events.hatch_complete += on_hatch_complete
