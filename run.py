"""
Entry script for running the performance tests
"""

import subprocess
import json
import csv
import glob
import os
import constants

def run_locust(locust, locust_count, hatch_rate, request_count):
    """
    Invokes locust on the command line
    """

    command = ['locust', '--no-web', '--only-summary',
               '-L', 'ERROR',
               '-c', str(locust_count),
               '-r', str(hatch_rate),
               '-n', str(request_count),
               locust]

    # If a locust path is specified, we will use it instead of what's on PATH
    if constants.LOCUST_PATH:
        command[0] = constants.LOCUST_PATH

    print 'Stress testing %s: %r' % (constants.HOST, command)
    return_code = subprocess.call(command, shell=True)

    if return_code != 0:
        raise Exception("Failed to invoke locust! Error code: %r", return_code)

def gather_reports():
    """
    Gathers all part reports from the locust runs and summarize them
    into one final statistics report
    """

    # Since glob.glob only works in cwd, we need to move into the results folder
    cwd = os.getcwd()
    os.chdir(constants.RESULTS_DIR)

    reports = list()
    for path in glob.glob(constants.REPORT_FILE_PREFIX + '*.json'):
        with open(path, 'r+') as report_file:
            report = json.load(report_file)
            reports.append(report)

    # Restore cwd
    os.chdir(cwd)
    return reports

def generate_csv(reports):
    """
    Generate a .csv file with all performance test results for plotting in Excel
    """
    report_path = os.path.join(constants.RESULTS_DIR, "report.csv")
    with open(report_path, 'w+') as csv_file:
        csv_writer = csv.writer(csv_file)
        headers = list()
        for report in reports:
            # Only write the headers once
            if not headers:
                headers = report.keys()

                # Move the endpoint name to the front for readability
                headers.insert(0, headers.pop(headers.index('name')))
                csv_writer.writerow(headers)

            # Write row data
            row = [report[key] for key in headers]
            csv_writer.writerow(row)

def remove_old_reports():
    """
    Cleanup old JSON reports
    """

    if not os.path.isdir(constants.RESULTS_DIR):
        return

    # Since glob.glob only works in cwd, we need to move into the results folder
    cwd = os.getcwd()
    os.chdir(constants.RESULTS_DIR)

    for path in glob.glob(constants.REPORT_FILE_PREFIX + '*.json'):
        print 'Cleaning up old report %s...' % path
        os.remove(path)

    # Restore cwd
    os.chdir(cwd)


def main():
    """
    Entry function
    """

    remove_old_reports()

    locust_increment = 10
    iterations = 50
    request_count = 1000
    locusts = ['Endpoint1Locust', 'Endpoint2Locust', 'Endpoint3Locust', 'Endpoint4Locust']

    for i in range(1, iterations + 1):
        locust_count = i * locust_increment
        hatch_rate = locust_count
        
        print '=' * 80
        print 'Test iteration %d / %d with locust count %d, hatch rate %d, request count %d' % (
            i, iterations, locust_count, hatch_rate, request_count)
        print '=' * 80

        for locust in locusts:
            run_locust(locust, locust_count, hatch_rate, request_count)

    reports = gather_reports()
    generate_csv(reports)

if __name__ == '__main__':
    main()

